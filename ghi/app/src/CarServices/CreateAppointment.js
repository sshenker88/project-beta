import React, {useState, useEffect } from 'react';


function CreateAppointment(){
    const blankAppointment = {
        "vin": "",
        "owner": "",
        "time_of_service": "",
        "reason": "",
        "employee_number": "",
    }

    const [lastAdded, setLastAdded] = useState("")
    const [submited, setSubmited] = useState(false)
    const [formData, setFormData] = useState(blankAppointment)
    const [techs, setTechs] = useState([])

    const messageClasses = (submited) ?   'alert alert-success mb-0' : 'alert alert-success d-none mb-0';

    const getTechsData = async () => {
        const locations_url = "http://localhost:8080/api/technicians/"
        const response = await fetch(locations_url);

        if (response.ok) {
          const data = await response.json();
          setTechs(data);
        }
    }


    const handleSubmit = async (e) => {
        e.preventDefault();
        const d = new Date(formData["time_of_service"]);
        formData["time_of_service"] = d.toISOString()
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers:{
                'Content-Type': 'application/json'
            }
        }
        const createURL = "http://localhost:8080/api/services/"
        const response = await fetch(createURL, fetchConfig)
        if (response.ok){
            setLastAdded(formData.owner)
            setSubmited(true)
            setFormData(blankAppointment)
        }
    }


    const handleFormChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }


    useEffect((() => {
        getTechsData();
    }), []);

    return (<>
    <h3 className={messageClasses} >Added appointment for {lastAdded}<link to="appointments/"></link></h3>
    <div className="row">
    <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
    <h1>Create an Appointment</h1>
    <form onSubmit={handleSubmit} id="create-appointment-form">
        <div className="form-floating mb-3">
            <input value={formData.vin} onChange={handleFormChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
            <label htmlFor="vin">VIN</label>
        </div>
        <div className="form-floating mb-3">
            <input value={formData.owner} onChange={handleFormChange} placeholder="Owner" required type="text" name="owner" id="owner" className="form-control" />
            <label htmlFor="owner">Owner</label>
        </div>
        <div className="form-floating mb-3">
            <input value={formData.time_of_service} onChange={handleFormChange} placeholder="Time of service" required type="datetime-local" name="time_of_service" id="time_of_service" className="form-control" />
            <label htmlFor="time_of_service">Time of Service</label>
        </div>
        <div className="form-floating mb-3">
            <input value={formData.reason} onChange={handleFormChange} placeholder="Reason for Service" required type="text" name="reason" id="reason" className="form-control" />
            <label htmlFor="reason">Reason for service</label>
        </div>
        <div className="mb-3">
            <select value={formData.location} onChange={handleFormChange} required name="employee_number" id="employee_number" className="form-select">
                        <option value="">Select technician</option>
                        {techs.map( tech => {
                            return (
                                <option key={tech.employee_number} value={tech.employee_number}>
                                {tech.name}
                                </option>
                            );
                        })}
            </select>
        </div>
        <button className="btn btn-primary">Create Appointment</button>
    </form>
</div>
</div>
</div>
</>
)
}


export default CreateAppointment
