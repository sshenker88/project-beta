import React, {useState, useEffect } from 'react';


function FinishedAppointments(){
    const [appts, setAppts] = useState([])
    const [searchForm, setSearchForm] = useState("")
    const [searchType, setsearchType] = useState("vin")

    const gettApptsData = async () => {
        const appts_url = "http://localhost:8080/api/services/"
        const apptsResponse = await fetch(appts_url)

        if (apptsResponse.ok){
            const apptsData = await apptsResponse.json()
            setAppts(apptsData)
        }
    }

    useEffect((() => {
        gettApptsData();
    }), []);


    const handleSearchUpdate = (e) => {
        setSearchForm({[e.target.name]: e.target.value.toLowerCase()})
    }

    const searchFilter = (a) => {
        if (searchType !== "Technician"){
            return a.completed && (a[searchType.toLowerCase()].toLowerCase().includes(searchForm.search) || searchForm.search === "" || searchForm === "")
        } else {
            return a.completed && (a[searchType.toLowerCase()]["name"].toLowerCase().includes(searchForm.search) || searchForm.search === "" || searchForm === "")
        }
    }

    const handleSearchTypeUpdate = (newTerm) =>{
        setsearchType(newTerm)
    }


    const dateParams = {
        hour12: true,
        hour:'numeric',
        minute: '2-digit',
        year: "numeric",
        month: "short",
        day: "numeric"
    }
    const vip_url = "https://i.pinimg.com/originals/ff/d4/17/ffd4173c82b31b140f2428f7a5dc5c28.gif"

    return <>
        <div className="">
            <div className="col-10">
                <div className="shadow p-4 mt-4">
                    <h1>Completed appointments</h1>
                    <div className="input-group mb-3 mt-3">
                        <input value={searchForm.name} onChange={handleSearchUpdate} placeholder="Search" required type="text" name="search" id="search" className="form-control" />
                        <label htmlFor="search"></label>
                        <div class="input-group-append">
                            <div className="dropdown">
                                <button className="btn btn-secondary btn-lg dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    Search by {searchType}
                                </button>
                                <ul className="dropdown-menu">
                                    <li><button className="dropdown-item" type="button" onClick={() => handleSearchTypeUpdate("VIN")}>VIN</button></li>
                                    <li><button className="dropdown-item" type="button" onClick={() => handleSearchTypeUpdate("Owner")}>Owner</button></li>
                                    <li><button className="dropdown-item" type="button" onClick={() => handleSearchTypeUpdate("Reason")}>Reason</button></li>
                                    <li><button className="dropdown-item" type="button" onClick={() => handleSearchTypeUpdate("Technician")}>Technician</button></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>VIN</th>
                                <th>Owner</th>
                                <th>Date and Time of Service</th>
                                <th>Reason for service</th>
                                <th>Technician</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                appts.filter(searchFilter).map( (appointment) => {
                                    let vip_td
                                    if (appointment.vip_status){
                                        vip_td = <img float="right" height="40" width="40" src={vip_url} alt="VIP"/>
                                    } else {
                                        vip_td = <></>
                                    }
                                    const time = new Date(appointment.time_of_service).toLocaleTimeString('en-US', dateParams)
                                    return (
                                    <tr key={appointment.id}>
                                        <td>{appointment.vin}</td>
                                        <td>{appointment.owner}{vip_td}</td>
                                        <td>{time}</td>
                                        <td>{appointment.reason}</td>
                                        <td>{appointment.technician.name}</td>
                                    </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        </>

}


export default FinishedAppointments
