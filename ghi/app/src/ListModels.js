import React, {useState, useEffect } from 'react';

function ListModels(){
    const [Model, setModel] = useState([])


    const getModelData = async ()  =>{
        const ModelURL = "http://localhost:8100/api/models/"
        const ModelResponse = await fetch(ModelURL)
        if (ModelResponse.ok){
            const ModelData = await ModelResponse.json();
            setModel(ModelData.models)
        }
    }

    
    useEffect((() => {
        getModelData();
    }), []);

    return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Model</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {Model.map((m)=>{
                        return (
                            <tr key={m.id}>
                                <td>{m.name}</td>
                                <td>{m.manufacturer.name}</td>
                                <td><img width="400px" alt="car model" src={m.picture_url}/></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        )
}


export default ListModels;
