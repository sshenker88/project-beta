import React, {useState } from 'react';


function CreateTech(){
    const blankTech = {
        "name": "",
        "employee_number": ""
    }
    const [lastAdded, setLastAdded] = useState("")
    const [submited, setSubmited] = useState(false)
    const [formData, setFormData] = useState(blankTech)

    const messageClasses = (!submited) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';


    const handleSubmit = async (e) => {
        e.preventDefault();
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers:{
                'Content-Type': 'application/json'
            }
        }
        const createURL = "http://localhost:8080/api/technicians/"
        const response = await fetch(createURL, fetchConfig)
        if (response.ok){
            setLastAdded(formData.name)
            setSubmited(true)
            setFormData(blankTech)
        }
    }


    const handleFormChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }


    return (<>
    <h3 className={messageClasses} >Added {lastAdded} to technicians<link to="/shoes"></link></h3>
    <div className="row">
    <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
        <h1>Add a technician</h1>
        <form onSubmit={handleSubmit} id="create-man-form">
            <div className="form-floating mb-3">
                <input value={formData.name} onChange={handleFormChange} placeholder="Name" required
                    type="text" name="name" id="name" className="form-control"
                />
                <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
                <input value={formData.employee_number} onChange={handleFormChange} placeholder="Employee Number" required
                    type="number" name="employee_number" id="employee_number" className="form-control"
                />
                <label htmlFor="employee_number">Employee Number</label>
            </div>
            <button className="btn btn-primary">Add Employee</button>
        </form>
    </div>
    </div>
    </div>
    </>
    )
}


export default CreateTech
