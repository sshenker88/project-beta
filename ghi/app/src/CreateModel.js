import React, {useState, useEffect } from 'react';


function CreateModel(){
    const blankModel = {
        "name": "",
        "picture_url": "",
        "manufacturer_id": ""
    }

    const [formData, setFormData] = useState(blankModel)
    const [mans, setMans] = useState([blankModel])


    const getMansData = async ()  =>{
        const mansURL = "http://localhost:8100/api/manufacturers/"
        const mansResponse = await fetch(mansURL)
        if (mansResponse.ok){
            const mansData = await mansResponse.json();
            setMans(mansData.manufacturers)
        }
    }

    
    const handleSubmit = async (e) => {
        e.preventDefault();
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers:{
                'Content-Type': 'application/json'
            }
        }
        const createURL = "http://localhost:8100/api/models/"
        const response = await fetch(createURL, fetchConfig)
        if (response.ok){
            setFormData(blankModel)
        }
    }


    const handleFormChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    useEffect((() => {
        getMansData();
    }), []);

    return (
    <div className="row">
    <div className="offset-3 col-6">
    <div className="shadow p-4 mt-4">
        <h1>Create a Model</h1>
        <form onSubmit={handleSubmit} id="create-man-form">
            <div className="form-floating mb-3">
            <input value={formData.name} onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
            <label htmlFor="name">Name</label>
            </div>
            <div className="form-floating mb-3">
            <input value={formData.picture_url} onChange={handleFormChange} placeholder="Picture" required type="url" name="picture_url" id="picture_url" className="form-control" />
            <label htmlFor="picture_url">Picture URL</label>
            </div>
            <div className="mb-3">
            <select value={formData.manufacturer_id} onChange={handleFormChange} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                <option value="">Select a Manufacturer</option>
                {mans.map( man => {
                    return (
                        <option key={man.id} value={man.id}>
                        {man.name}
                        </option>
                    );
                })}
            </select>
            </div>
            <button className="btn btn-primary">Create</button>
        </form>
    </div>
    </div>
    </div>
)

}


export default CreateModel
