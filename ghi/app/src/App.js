import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import ListManufacturers from './ListManufacturers';
import Nav from './Nav';
import CreateManufacturer from './CreateManufacturer';
import ListModels from './ListModels';
import ListAutos from './ListAutomobiles';
import CreateModel from './CreateModel';
import CreateAuto from './CreateAuto';
import CreateTech from './CarServices/CreateTech';
import ListAppointments from './CarServices/ListAppointments';
import FinishedAppointments from './CarServices/FinishedAppointments';
import CreateAppointment from './CarServices/CreateAppointment';

function App() {
  return (
    <>
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/manufacturers/" element ={<ListManufacturers />} />
          <Route path="/manufacturers/create/" element={<CreateManufacturer/>}/>
          <Route path="/models/" element ={<ListModels />} />
          <Route path="/models/create/" element={<CreateModel/>}/>
          <Route path="/autos/" element ={<ListAutos />} />
          <Route path="/autos/create/" element ={<CreateAuto />} />
          <Route path="/technicians/create/" element ={<CreateTech />} />
          <Route path="/appointments/" element = {<ListAppointments />} />
          <Route path="/appointments/finished/" element = {<FinishedAppointments/>} />
          <Route path="/appointments/create/" element = {<CreateAppointment/>} />
        </Routes>
      </div>
    </BrowserRouter>
    </>
  );
}

export default App;
