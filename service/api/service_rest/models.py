from django.db import models


class AutoVO(models.Model):
    vin = models.CharField(max_length=200, unique=True)


class Technician(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.IntegerField(unique=True)

class Appointment(models.Model):
    vin = models.CharField(max_length=200)
    owner = models.CharField(max_length=200)
    time_of_service = models.DateTimeField()
    reason = models.TextField()
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE
    )
    completed=models.BooleanField(default=False)
    vip_status = models.BooleanField(default=False)
