from common.json import ModelEncoder
from ..models import Technician, AutoVO, Appointment

class AutoVOEncoder(ModelEncoder):
    model = AutoVO
    properties = [
        "vin"
    ]

class TechEncoder(ModelEncoder):
    model=Technician
    properties = [
        "name",
        "employee_number",
    ]

class AppointmentEndoder(ModelEncoder):
    model=Appointment
    properties = [
        "vin",
        "owner",
        "time_of_service",
        "reason",
        "completed",
        "id",
        "vip_status"
    ]
    def get_extra_data(self, o):
        return {
            "technician": {
            "name": o.technician.name,
            "employee_number": o.technician.employee_number
            }
        }
