import React, {useState, useEffect } from 'react';

function ListManufacturers(){
    const [mans, setMans] = useState([])


    const getMansData = async ()  =>{
        const mansURL = "http://localhost:8100/api/manufacturers/"
        const mansResponse = await fetch(mansURL)
        if (mansResponse.ok){
            const mansData = await mansResponse.json();
            setMans(mansData.manufacturers)
        }
    }

    
    useEffect((() => {
        getMansData();
    }), []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>name</th>
                </tr>
            </thead>
            <tbody>
                {mans.map((man) =>{
                    return (
                        <tr key={man.id}>
                            <td>{man.name}</td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
        )
}


export default ListManufacturers;
