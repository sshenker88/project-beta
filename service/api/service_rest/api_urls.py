from django.contrib import admin
from django.urls import path, include
from .views import api_list_techs, api_list_appointments, api_update_appointment
urlpatterns = [
    path("technicians/", api_list_techs, name="api_list_techs"),
    path("services/", api_list_appointments, name="api_list_services" ),
    path("services/<int:id>/", api_update_appointment, name="api_update_appointment"),
]
