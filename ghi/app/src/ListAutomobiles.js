import React, {useState, useEffect } from 'react';

function ListAutos(){
    const [auto, setAuto] = useState([])


    const getAutoData = async ()  =>{
        const autoURL = "http://localhost:8100/api/automobiles/"
        const autoResponse = await fetch(autoURL)
        if (autoResponse.ok){
            const autoData = await autoResponse.json();
            setAuto(autoData.autos)
        }
    }


    useEffect((() => {
        getAutoData();
    }), []);

    return (
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>VIN</th>
                    <th>Color</th>
                    <th>Year</th>
                    <th>Model</th>
                    <th>Manufacturer</th>
                </tr>
            </thead>
            <tbody>
                {auto.map( (a) =>{
                    return (
                    <tr key={a.vin}>
                        <td>{a.vin}</td>
                        <td>{a.color}</td>
                        <td>{a.year}</td>
                        <td>{a.model.name}</td>
                        <td>{a.model.manufacturer.name}</td>
                    </tr>
                    );
                })}
            </tbody>
        </table>
        )
}


export default ListAutos;
