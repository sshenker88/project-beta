import React, {useState } from 'react';


function CreateManufacturer(){
    const blankMan = {"name": ""}
    const [formData, setFormData] = useState(blankMan)


    const handleSubmit = async (e) => {
        e.preventDefault();
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(formData),
            headers:{
                'Content-Type': 'application/json'
            }
        }
        const createURL = "http://localhost:8100/api/manufacturers/"
        const response = await fetch(createURL, fetchConfig)
        if (response.ok){
            setFormData(blankMan)
        }
    }

    
    const handleFormChange = (e) => {
        setFormData({...formData, [e.target.name]: e.target.value});
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a Manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-man-form">
                <div className="form-floating mb-3">
                    <input value={formData.name} onChange={handleFormChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                    <label htmlFor="name">Name</label>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
        </div>
    )
}


export default CreateManufacturer
