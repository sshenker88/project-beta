import {  NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li><NavLink className="navbar-brand" to="/manufacturers/" >Manufacturers</NavLink></li>
            <li><NavLink className="navbar-brand" to="/manufacturers/create/" >Create a Manufacturer</NavLink></li>
            <li><NavLink className="navbar-brand" to="/models/" >Models</NavLink></li>
            <li><NavLink className="navbar-brand" to="/models/create/" >Create a Model</NavLink></li>
            <li><NavLink className="navbar-brand" to="/autos/" >List Autos</NavLink></li>
            <li><NavLink className="navbar-brand" to="/autos/create/" >Create an Auto</NavLink></li>
          </ul>
        </div>
      </div>
      <div className="collapse navbar-collapse" id="navbarSupportedContent">
        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <li><NavLink className="navbar-brand" to="/technicians/create/" >Add Technician</NavLink></li>
          <li><NavLink className="navbar-brand" to= "appointments/" >Appointments</NavLink></li>
          <li><NavLink className="navbar-brand" to= "appointments/finished/" >Finished Appointments</NavLink></li>
          <li><NavLink className="navbar-brand" to= "appointments/create/" >Create Appointment</NavLink></li>
        </ul>
      </div>
    </nav>
  )
}

export default Nav;
