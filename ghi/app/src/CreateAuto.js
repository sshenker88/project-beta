import React, {useState, useEffect } from 'react';


function CreateAuto(){
    const blankAuto = {
        "color": "",
        "year": "",
        "vin": "",
        "manufacturer_id": "",
        "model": "",
    }

    const [formData, setFormData] = useState(blankAuto)
    const [mans, setMans] = useState([])
    const [models, setModels] = useState([])

    const fadedClasses = (models.length===0 || formData.manufacturer_id === "") ? 'd-none' : "mb-3";
    const hintClasses = (formData.manufacturer_id === "") ? "mb-3" : "d-none";
    const noCarsClasses = (models.length===0 && formData.manufacturer_id !== "") ? "mb-3" : "d-none";
    const fadedButton = (formData.model === "") ? "btn btn-primary disabled" : 'btn btn-primary'


    const getMansData = async ()  =>{
        const mansURL = "http://localhost:8100/api/manufacturers/"
        const mansResponse = await fetch(mansURL)
        if (mansResponse.ok){
            const mansData = await mansResponse.json();
            setMans(mansData.manufacturers)
        }
    }


    const getModelsData = async (man_id)  =>{
        const modelURL = "http://localhost:8100/api/models/"
        const modelResponse = await fetch(modelURL)
        if (modelResponse.ok){
            const modelData = await modelResponse.json();
            const filtered = modelData.models.filter(model => {
                return model.manufacturer.id == man_id;
            })
            setModels(filtered)
        }
    }


    const handleSubmit = async (e) => {
        e.preventDefault();
        const cleanForm = {
            "color": formData.color,
            "year": formData.year,
            "vin": formData.vin,
            "model_id": formData.model
        }
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(cleanForm),
            headers:{
                'Content-Type': 'application/json'
            }
        }
        const createURL = "http://localhost:8100/api/automobiles/"
        const response = await fetch(createURL, fetchConfig)
        if (response.ok){
            setFormData(blankAuto)
        }
    }


    const handleFormChange = (e) => {
        if (e.target.name === "manufacturer_id"){
            setFormData({...formData, "model": "", [e.target.name]: e.target.value})
            getModelsData(e.target.value)
        } else {
            setFormData({...formData, [e.target.name]: e.target.value});
        }
    }

    useEffect(( () => {
        getMansData();
    }), []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Add an automobile to inventory</h1>
            <form onSubmit={handleSubmit} id="create-auto-form">
                <div className="form-floating mb-3">
                    <input value={formData.color} onChange={handleFormChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                    <label htmlFor="color">Color</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.year} onChange={handleFormChange} placeholder="Year" required type="number" name="year" id="year" className="form-control" />
                    <label htmlFor="year">Year</label>
                </div>
                <div className="form-floating mb-3">
                    <input value={formData.vin} onChange={handleFormChange} placeholder="VIN" required type="text" name="vin" id="vin" className="form-control" />
                    <label htmlFor="vin">VIN</label>
                </div>
                <div className="mb-3">
                    <select value={formData.manufacturer_id} onSelect={handleFormChange} onChange={handleFormChange} required name="manufacturer_id" id="manufacturer_id" className="form-select">
                        <option value="">Select a Manufacturer</option>
                        {mans.map( man => {
                            return (
                                <option key={man.id} value={man.id}>{man.name}</option>
                            );
                        })}
                    </select>
                </div>
                <div className={hintClasses}>Select a manufacturer before model</div>
                <div className={noCarsClasses}>Out of stock from that manufacturer, Try a different one</div>
                <div className={fadedClasses}>
                    <select  value={formData.model} onChange={handleFormChange} required name="model" id="model" className="form-select">
                        <option value="">Select a Model</option>
                        {models.map( model => {
                            return (
                                <option key={model.id} value={model.id}>{model.name}</option>
                            );
                        })}
                    </select>
                    <div className="mb-3"/>
                </div>
                <button className={fadedButton} >Create</button>
            </form>
        </div>
        </div>
        </div>
    )
}

export default CreateAuto
