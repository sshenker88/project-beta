# CarCar

Team:

* Sam Shenker - Automobile Service
* Person 2 - N/A no sales service included

## Design

(See Excalidraw service overview file)

there are four main parts in the whole project.
1. The inventory service (provided to us)
2. The car Service microservice
3. The car Sales microservice (not attempted due to no group member)
4. The React front end service.

The inventory service was provided to us and we were instructed to not
make any changes so it would be its own bounded context. Just use the APIs

The car service microservice has a poller microservice inside its
bounded context as well and has an API for the react front end.
It keeps a value object from the Automobile model in the inventory
service with the vin number.

The react front end accesses both the inventory service as well as
the car Service microservice. it would also access the sales one presumably

## Service microservice

Explain your models and integration with the inventory
microservice, here.

I created three models.

- AutoVO which only has the vin (stored as unique)
- Technician with a name and a unique employee number
- Appointment with vin (not a foreign key nor unique), owner, time of service, reason, technician (which is a foreign key), completed Boolean, and vip_status Boolean

The vip_status and completed fields on the Appointment models default to false.
However, when the list appointment view function gets a request to return the
list of appointments, it compares all vins from in the inventory microservice(via its own autoVO model) with those in the services/appointments microservice and
changes ands saves the vip_status inside the Appointment model before passing
along the data. When a PUT request is made to the microservice, it changes the
completed status to True. When a DELETE request is made to the microservice,
the appointments are instead deleted.

The front end filters out which services are completed to show for completed or
history of appointments. The VIP status is checked when mapping through the
returned list of appointments and returns a different owner name if they're VIP.

The react front end can create new technicians, appointments, and interface with
the inventory backend to create new automobiles, list them, list the models, create
a new model, list the manufacturers, and add a manufacturer

## Sales microservice

Explain your models and integration with the inventory
microservice, here.

Womp womp
