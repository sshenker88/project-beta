import React, {useState, useEffect } from 'react';

function ListAppointments(){
    const [appts, setAppts] = useState([])
    const [OGAppts, setOGAppts] = useState([])


    const gettApptsData = async () => {
        const appts_url = "http://localhost:8080/api/services/"
        const apptsResponse = await fetch(appts_url)
        
        if (apptsResponse.ok){
            const apptsData = await apptsResponse.json()
            setOGAppts(apptsData)
            setAppts(apptsData)
        }
    }


    const sortByDate = () => {
        const apptsDataDates = appts.map(appt => {
            const apptdate = new Date(appt["time_of_service"])
            appt["time_of_service"] = apptdate
            return appt
        })
        apptsDataDates.sort((a, b) => a["time_of_service"] - b["time_of_service"])
        setAppts(apptsDataDates)
    }


    const sortByOwner = () => {
        setAppts(OGAppts)
    }


    const sortByTech = () => {
        const apptsTechSorted = appts.map(appt => appt)
        apptsTechSorted.sort((a, b) =>
            a.technician.name.localeCompare(b.technician.name));
        setAppts(apptsTechSorted)
    }


    useEffect((() => {
        gettApptsData();
    }), []);


    const handleDelete = async (e) => {
        const url = `http://localhost:8080/api/services/${e.target.id}/`
        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }
        const delResponse = await fetch(url, fetchConfigs)
        const delData = await delResponse.json()
        gettApptsData()
    }


    const handleFinished = async (e) => {
        const url = `http://localhost:8080/api/services/${e.target.id}/`
        const fetchConfigs = {
            method: "Put",
            headers: {
                "Content-Type": "application/json"
            }
        }
        const putResponse = await fetch(url, fetchConfigs)
        const putData = await putResponse.json()
        gettApptsData()
    }

    const vip_url = "https://i.pinimg.com/originals/ff/d4/17/ffd4173c82b31b140f2428f7a5dc5c28.gif"
    const dateParams = {
        hour12: true,
        hour:'numeric',
        minute: '2-digit',
        year: "numeric",
        month: "short",
        day: "numeric"
    }
    return <>
            <div className="">
                <div className="offset-1 col-12">
                    <div className="shadow p-4 mt-4">
                        <h1>Appointments</h1>
                        <table className="table table-striped">
                            <thead>
                                <tr>
                                    <th>VIN</th>
                                    <th onClick={sortByOwner}>Owner</th>
                                    <th onClick={sortByDate}>Date and Time of Service</th>
                                    <th>Reason for service</th>
                                    <th onClick={sortByTech}>Technician</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    appts.filter(appointment => !appointment.completed).map( (appointment) => {
                                        let vip_td
                                        if (appointment.vip_status){
                                            vip_td = <td>{appointment.owner}<img float="right" height="40" width="40" src={vip_url} alt="VIP"/></td>
                                        } else {
                                            vip_td = <td>{appointment.owner}</td>
                                        }
                                        const time = new Date(appointment.time_of_service).toLocaleDateString('en-US', dateParams)
                                        return (
                                        <tr key={appointment.id}>
                                            <td>{appointment.vin}</td>
                                            {vip_td}
                                            <td>{time}</td>
                                            <td>{appointment.reason}</td>
                                            <td>{appointment.technician.name}</td>
                                            <td>
                                                <button onClick={handleDelete} id={appointment.id} className="btn btn-lg btn-warning ">Cancel</button>
                                                <button onClick={handleFinished} id={appointment.id} className="btn btn-lg btn-success ">Finished</button>
                                            </td>
                                        </tr>
                                        );
                                    }
                                    )
                                }
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </>

}


export default ListAppointments
