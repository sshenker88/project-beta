from django.contrib import admin
from service_rest.models import Appointment, Technician, AutoVO

# Register your models here.


@admin.register(Appointment)
class TaskAdmin(admin.ModelAdmin):
    list_display = (
        "vin",
        "owner",
        "completed",
        "vip_status",
    )
