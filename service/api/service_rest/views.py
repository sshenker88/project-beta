from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
from .models import Technician, AutoVO, Appointment
from .encoders.encoders import *
import json


@require_http_methods(["PUT", "DELETE"])
def api_update_appointment(request, id=None):
    if request.method == "PUT": # Changes completed to True, doesn't change it back
        try:
            appt = Appointment.objects.get(id=id)
            appt.completed = True
            appt.save()
            return JsonResponse(
                appt,
                encoder=AppointmentEndoder,
                safe=False
                )
        except Appointment.DoesNotExist:
            return JsonResponse({"Appointment does not exist":"appointment does not exist"})
    if request.method == "DELETE":
        try:
            appt = Appointment.objects.filter(id=id)
            deleted, _ = appt.delete()
            return JsonResponse({"Deleted": f'{deleted > 0}'})
        except Exception:
            return JsonResponse({
                "Delete failure": Exception
                })


@require_http_methods(["GET", "POST"])
def api_list_techs(request):
    if request.method == "GET":
        try:
            techs = Technician.objects.all()
            return JsonResponse(
                techs,
                encoder=TechEncoder,
                safe=False
            )
        except Exception:
            return JsonResponse({"error with GET": Exception})
    if request.method == "POST":
        content = json.loads(request.body)
        if Technician.objects.filter(employee_number=content["employee_number"]):
            return JsonResponse({"Duplicate employee number": 'That employee already exists'})
        try:
            tech = Technician.objects.create(**content)
            return JsonResponse(
                tech,
                encoder=TechEncoder,
                safe=False
            )
        except Exception:
            return JsonResponse({"error with POST": Exception})


@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == "POST":
        content = json.loads(request.body)
        try:
            tech = Technician.objects.get(employee_number=content["employee_number"])
            appt_details = {
                "vin": content["vin"],
                "owner": content["owner"],
                "time_of_service": content["time_of_service"],
                "reason": content["reason"],
                "technician": tech,
            }
            appt = Appointment.objects.create(**appt_details)
            return JsonResponse(
                appt,
                encoder=AppointmentEndoder,
                safe=False
            )
        except Technician.DoesNotExist:
            return JsonResponse({"Can't find employee number": "Double Check your employee number"})
    elif request.method == "GET":
        appts = []
        vins = []
        try:
            appts = Appointment.objects.all().order_by("owner")
        except Exception:
            return JsonResponse({"error in appts request": Exception})
        try:
            vins = AutoVO.objects.all()
        except Exception:
            return JsonResponse({"error in AutoVO request": Exception})
        
        vin_list = [vin.vin for vin in vins]
        for ap in appts:
            if ap.vin in vin_list:
                ap.vip_status = True
                ap.save()
        return JsonResponse(
            appts,
            encoder=AppointmentEndoder,
            safe=False,
        )
